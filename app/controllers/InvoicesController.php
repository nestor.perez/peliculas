<?php

use Phalcon\Flash;
use Phalcon\Session;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

/**
 * InvoicesController
 *
 * Manage operations for invoises
 */
class InvoicesController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Administra las Películas');
        parent::initialize();
    }

    public function indexAction()
    {
		$this->session->conditions = null;
        $this->view->form = new InvoicesForm;
    }

    /**
     * Edit the active user profile
     *
     */
    public function profileAction()
    {
        //Get session info
        $auth = $this->session->get('auth');

        //Query the active user
        $user = Users::findFirst($auth['id']);
        if ($user == false) {
            return $this->dispatcher->forward(
                [
                    "controller" => "index",
                    "action"     => "index",
                ]
            );
        }

        if (!$this->request->isPost()) {
            $this->tag->setDefault('name', $user->name);
            $this->tag->setDefault('email', $user->email);
        } else {

            $name = $this->request->getPost('name', ['string', 'striptags']);
            $email = $this->request->getPost('email', 'email');

            $user->name = $name;
            $user->email = $email;
            if ($user->save() == false) {
                foreach ($user->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
            } else {
                $this->flash->success('Su información ha sido actualizada con éxito');
            }
        }
    }
	
	 public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Invoices", $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $invoices = Invoices::find($parameters);
        if (count($invoices) == 0) {
            $this->flash->notice("No se encuentran películas");

            return $this->dispatcher->forward(
                [
                    "controller" => "invoices",
                    "action"     => "index",
                ]
            );
        }

        $paginator = new Paginator(array(
            "data"  => $invoices,
            "limit" => 10,
            "page"  => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Shows the form to create a new product
     */
    public function newAction()
    {
        $this->view->form = new InvoicesForm(null, array('edit' => true));
    }

    /**
     * Edits a product based on its id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $invoice = Invoices::findFirstById($id);
            if (!$invoice) {
                $this->flash->error("Película no encontrada");

                return $this->dispatcher->forward(
                    [
                        "controller" => "invoices",
                        "action"     => "index",
                    ]
                );
            }

            $this->view->form = new InvoicesForm($invoice, array('edit' => true));
        }
    }

    /**
     * Creates a new product
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                [
                    "controller" => "invoices",
                    "action"     => "index",
                ]
            );
        }

        $form = new InvoicesForm;
        $invoice = new Invoices();

        $data = $this->request->getPost();
        if (!$form->isValid($data, $invoice)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "invoices",
                    "action"     => "new",
                ]
            );
        }

        if ($invoice->save() == false) {
            foreach ($invoice->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "invoices",
                    "action"     => "new",
                ]
            );
        }

        $form->clear();

        $this->flash->success("Pelicula creada con éxito");

        return $this->dispatcher->forward(
            [
                "controller" => "invoices",
                "action"     => "index",
            ]
        );
    }

    /**
     * Saves current product in screen
     *
     * @param string $id
     */
    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                [
                    "controller" => "invoices",
                    "action"     => "index",
                ]
            );
        }

        $id = $this->request->getPost("id", "int");

        $invoice = Invoices::findFirstById($id);
        if (!$invoice) {
            $this->flash->error("Película no existe");

            return $this->dispatcher->forward(
                [
                    "controller" => "invoices",
                    "action"     => "index",
                ]
            );
        }

        $form = new InvoicesForm;
        $this->view->form = $form;

        $data = $this->request->getPost();

        if (!$form->isValid($data, $invoice)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "invoices",
                    "action"     => "edit",
                    "params"     => [$id]
                ]
            );
        }

        if ($invoice->save() == false) {
            foreach ($invoice->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "invoices",
                    "action"     => "edit",
                    "params"     => [$id]
                ]
            );
        }

        $form->clear();

        $this->flash->success("Película actualizada con éxito");

        return $this->dispatcher->forward(
            [
                "controller" => "invoices",
                "action"     => "index",
            ]
        );
    }

    /**
     * Deletes a product
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $invoice = Invoices::findFirstById($id);
        if (!$invoice) {
            $this->flash->error("Pelicula no existe");

            return $this->dispatcher->forward(
                [
                    "controller" => "invoices",
                    "action"     => "index",
                ]
            );
        }

        if (!$invoice->delete()) {
            foreach ($invoice->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "invoices",
                    "action"     => "search",
                ]
            );
        }

        $this->flash->success("Pelicula ha sido eliminada");

            return $this->dispatcher->forward(
                [
                    "controller" => "invoices",
                    "action"     => "index",
                ]
            );
    }
}
