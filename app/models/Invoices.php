<?php

use Phalcon\Mvc\Model;

/**
 * INVOICES
 */
class Invoices extends Model
{
	/**
	 * @var integer
	 */
	public $id;

	/**
	 * @var integer
	 */
	public $id_Director;

	/**
	 * @var string
	 */
	public $titulo;

	/**
	 * @var string
	 */
	public $desp;

	/**
	 * @var string
	 */
	
	public function initialize()
	{
		$this->belongsTo('id_Director', 'Companies', 'id', [
			'reusable' => true
		]);
	}

	
}
