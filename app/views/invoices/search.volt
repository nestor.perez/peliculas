{{ content() }}

<ul class="pager">
    <li class="previous">
        {{ link_to("invoices", "&larr; Go Back") }}
    </li>
    <li class="next">
        {{ link_to("invoices/new", "Nueva Película") }}
    </li>
</ul>

{% for invoices in page.items %}
    {% if loop.first %}
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Titulo</th>
            <th>Descripción</th>
           	<th>Director</th>
           
        </tr>
    </thead>
    <tbody>
    {% endif %}
        <tr>
            <td>{{ invoices.id }}</td>
            <td>{{ invoices.titulo }}</td>
            <td>{{ invoices.desp }}</td>
			<td>{{ invoices.getCompanies().name }}</td>
			
            <td width="7%">{{ link_to("invoices/delete/" ~ invoices.id, '<i class="glyphicon glyphicon-remove"></i> Borrar', "class": "btn btn-default") }}</td>
        </tr>
    {% if loop.last %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="7" align="right">
                <div class="btn-group">
                    {{ link_to("invoices/search", '<i class="icon-fast-backward"></i> Primero', "class": "btn") }}
                    {{ link_to("invoices/search?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                    {{ link_to("invoices/search?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                    {{ link_to("invoices/search?page=" ~ page.last, '<i class="icon-fast-forward"></i> Último', "class": "btn") }}
                    <span class="help-inline">{{ page.current }} of {{ page.total_pages }}</span>
                </div>
            </td>
        </tr>
    </tbody>
</table>
    {% endif %}
{% else %}
    No existen actores
{% endfor %}
