<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Numericality;

class InvoicesForm extends Form
{
    /**
     * Initialize the products form
     */
    public function initialize($entity = null, $options = array())
    {
        if (!isset($options['edit'])) {
            $element = new Text("id");
            $this->add($element->setLabel("Id"));
        } else {
            $this->add(new Hidden("id"));
        }

        $name = new Text("titulo");
        $name->setLabel("Titulo Película");
        $name->setFilters(['striptags', 'string']);
        $name->addValidators([
            new PresenceOf([
                'message' => 'Titulo de Película requerido'
            ])
        ]);
        $this->add($name);
		
		$desp = new Text("desp");
        $desp->setLabel("Descripción de Película");
        $desp->setFilters(['striptags', 'string']);
        $desp->addValidators([
            new PresenceOf([
                'message' => 'Descripcion de Película requerido'
            ])
        ]);
        $this->add($desp);

        $type = new Select('id_Director', Companies::find(), [
            'using'      => ['id', 'name'],
            'useEmpty'   => true,
            'emptyText'  => '...',
            'emptyValue' => ''
        ]);
        $type->setLabel('Director');
        $this->add($type);

      
        
    }
}
